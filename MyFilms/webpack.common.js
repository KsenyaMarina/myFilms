﻿const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const distPath = path.resolve(__dirname, 'wwwroot/dist');

module.exports = {
    entry: {
        index: './src/index.js'
    },
    output: {
        filename: '[name].js',
        path: distPath
    },
    plugins: [
        // cleans dist dir after each recompile
        new CleanWebpackPlugin([distPath], { watch: true })
    ]
};