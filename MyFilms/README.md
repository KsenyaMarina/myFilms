﻿# AspNetCore.WebpackIntegration

Given repo provides an example of how we can create a lightweight template of empty ASP.NET Core (without Bootstrap) & Webpack combination.

## Details

1. It uses Razor Pages
2. It uses default location (wwwroot) for static files
3. It compiles index.js file from ./src directory into ./wwwroot/dist directory
4. Compiled files are placed in ./wwwroot/dist directory
5. It supports multiple config environments (dev & prod) using "webpack-merge" webpack plugin.
6. NPM command "watch" uses webpack.dev.js config and should be used in development for autocompile.
7. NPM command "build" uses webpack.prod.js config and should be used to compile .js files for production use.


## Requirements

1. Node.js with NPM
2. .NET Core SDK (2.1)

## How to get started

Note 1: Visual Studio 2017 will should do step 1 for you automatically.

Note 2: If you are using Visual Studio, you should add this project to your solution for better IntelliSense

Steps:

1. clone the repo ("git clone https://github.com/larestons/AspNetCore.WebpackIntegration.git")
3. Restore npm & .NET packages ("npm install & dotnet restore")
4. Run webpack in watch mode ("npm run watch" in terminal or with Task Runner with NPM extension (VS 2017))
5. Run .NET Core webserver ("dotnet run" in terminal or VS 2017 IIS Express)